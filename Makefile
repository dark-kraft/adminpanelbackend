up:
	docker-compose -f docker-compose.dev.yml up --build

down:
	docker-compose -f docker-compose.dev.yml down --remove-orphans

lock:
	docker-compose -f docker-compose.dev.yml run flask pipenv lock
