from flask import Flask
from flask_redis import FlaskRedis
from flask_marshmallow import Marshmallow

from config import FlaskConfig

redis = FlaskRedis(decode_responses=True)
ma = Marshmallow()


def create_app() -> Flask:
    app = Flask(__name__)
    app.config.from_object(FlaskConfig)
    redis.init_app(app)
    ma.init_app(app)

    from resourses.redis_urls import UrlResource

    from blueprint import url_api_bp
    app.register_blueprint(url_api_bp)

    return app
