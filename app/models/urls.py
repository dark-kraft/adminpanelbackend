from marshmallow import Schema, fields


class UrlSchema(Schema):
    name = fields.Str(required=True, allow_none=False)
    url = fields.Str(required=True, allow_none=False)
    description = fields.Str(required=True, allow_none=False)
    alias = fields.Str(required=True, allow_none=False)
    updated = fields.Boolean(required=False, default=False)

