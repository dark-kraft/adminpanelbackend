from typing import *

from flask import request
from flask_restful import Resource
from marshmallow import Schema, ValidationError
from appfactory import redis
from blueprint import url_api
from models.urls import UrlSchema
from service.redis_get_url import UrlDispatch


@url_api.resource('/urls/<string:url>', '/urls')
class UrlResource(Resource):
    _url_service: UrlDispatch = UrlDispatch(redis=redis)
    _url_schema: Schema = UrlSchema()

    def get(self) -> Tuple[List[Dict], int]:
        result: List[Dict] = []
        urls_dict: Dict = self._url_service.get_all_urls()
        for key, value in urls_dict.items():
            result.append(self._url_schema.loads(value))  ## TODO предполагается, что тут не будет ошибки?
        return result, 200

    def put(self,  url: Optional[str] = None) -> Tuple[Dict, int]:
        if not url:
            return {'error': 'url must be specified'}, 400
        data: dict
        if not (data := request.get_json()):
            return {'error': 'No data'}, 400
        try:
            url_data: dict = self._url_schema.load(data)
            if self._url_service.url_exists(url):
                if url_data.get('updated'):
                    self._url_service.put_url(url, self._url_schema.dumps(url_data))
                    return {'Updated': 'OK'}, 200
                else:
                    return {'error': f'{url} уже существует'}, 400
            else:
                self._url_service.put_url(url, self._url_schema.dumps(url_data))
                return {'sucess': 'OK'}, 200
        except ValidationError as err:
            return {'error': err.messages}, 400

    def delete(self, url: Optional[str] = None) -> Tuple[Dict, int]:
        if not url:
            return {'error': 'url must be specified'}, 400
        if not self._url_service.url_exists(url):
            return {'error': f'{url} не существует'}, 400
        self._url_service.remove_url(url)
        return {'success': 'OK'}, 204
