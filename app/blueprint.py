from flask import Blueprint
from flask_restful import Api

url_api_bp = Blueprint('api', __name__, url_prefix='/api')
url_api = Api(url_api_bp)
