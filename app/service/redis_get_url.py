from typing import *
from redis import Redis
from models.urls import UrlSchema


class UrlDispatch:
    def __init__(self, redis: Redis):
        self.redis_cli: Redis = redis

    def get_all_urls(self) -> dict:
        keys = self.redis_cli.keys()
        urls: Dict = {}
        for key in keys:
            urls[key] = self.redis_cli.get(key)
        return urls

    def get_urls_by_env(self, env: str) -> dict:
        keys = self.redis_cli.keys(f'*:{env}')
        urls: Dict = {}
        for key in keys:
            urls[key] = self.redis_cli.get(key)
        return urls

    def url_exists(self, url: str) -> bool:
        return True if self.redis_cli.get(f'{url}') else False

    def put_url(self, url: str, json_data: str):
        self.redis_cli.set(f'{url.strip()}', json_data)

    def remove_url(self, url: str):
        obj = self.redis_cli.keys(f'{url.strip()}')[0]
        self.redis_cli.delete(obj)

