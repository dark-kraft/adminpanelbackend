from os import getenv


class FlaskConfig:
    FLASK_APP_SECRET_KEY = getenv('FLASK_APP_SECRET_KEY', 'Nope')
    REDIS_URL = getenv('REDIS_URL', 'redis://redis:6379')
    FLASK_ENV = getenv('FLASK_ENV', 'dev')
    PYTHONUNBUFFERED = getenv('PYTHONUNBUFFERED', '1')

