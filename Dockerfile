FROM python:3.9 as base
RUN pip3 install --upgrade pipenv==2021.5.29

FROM base as deps

COPY Pipfile* /AdminPanel/
WORKDIR /AdminPanel
RUN pipenv install --system --ignore-pipfile --deploy

FROM deps as app

COPY ./app/ /AdminPanel/
